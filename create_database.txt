Criar base:
create database vw_nome_da_base;
create user vw_nome_user identified by 'exemplo-de-senha-123';
grant all privileges on vw_nome_da_base.* to vw_nome_user@'%' identified by 'exemplo-de-senha-123';
flush privileges;

Alterar URL do projeto (ovk = prefixo de tabela):
select * from ovk_options where option_id=1;
update ovk_options set option_value = 'http://vw-urlprojeto-hml.ageriservicos.com.br' where option_id=1;
update ovk_options set option_value = 'http://vw-urlprojeto-hml.ageriservicos.com.br' where option_id=2;

Criar usuário de admin wordpress (ovk = prefixo de tabela):
INSERT INTO `ovk`.`ovk_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES ('X', 'admin', MD5('ageri2020'), 'Admin', 'suporte@ageri.com.br', '', '2020-09-09 09:00:00', '', '0', 'Admin');

INSERT INTO `ovk`.`ovk_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, 'X', 'ovk_capabilities', 'a:1:{s:13:"administrator";s:1:"1";}');
  
INSERT INTO `ovk`.`ovk_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, 'X', 'ovk_user_level', '10');

Obs: ID do usuario = X, alterar para um que não exista.